package auth

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
)

type Userlogin struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type Resauth struct {
	Success     bool   `json:"success"`
	Accesstoken string `json:"accessToken"`
	Msg         string `json:"msg"`
}

func Login(username string, password string, url string) Resauth {

	login := &Userlogin{
		Username: username,
		Password: password,
	}

	jsonReq, err := json.Marshal(login)
	resp, err := http.Post(url, "application/json; charset=utf-8", bytes.NewBuffer(jsonReq))
	if err != nil {
		log.Fatalln(err)
	}

	defer resp.Body.Close()
	bodyBytes, _ := ioutil.ReadAll(resp.Body)

	var resd Resauth
	if err := json.Unmarshal(bodyBytes, &resd); err != nil {
		panic(err)
	}

	return resd
}
