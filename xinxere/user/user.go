package user

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
)

type Userlogin struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type Usersignup struct {
	Name     string `json:"name"`
	Username string `json:"username"`
	Email    string `json:"email"`
	Password string `json:"password"`
	Source   string `json:"source"`
}

type Datauser struct {
	Id       int    `json:"id"`
	Name     string `json:"name"`
	Username string `json:"username"`
	Email    string `json:"email"`
}

type Resdata struct {
	Success bool     `json:"success"`
	Data    Datauser `json:"data"`
}

type Resmsg struct {
	Success bool   `json:"success"`
	Msg     string `json:"msg"`
}

func Login(username string, password string, url string, token string) Resdata {

	login := &Userlogin{
		Username: username,
		Password: password,
	}

	client := &http.Client{}
	jsonReq, err := json.Marshal(login)
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonReq))

	if err != nil {
		log.Fatalln(err)
	}

	req.Header.Add("Content-Type", "application/json; charset=utf-8")
	req.Header.Add("x-access-token", token)

	resp, err := client.Do(req)

	defer resp.Body.Close()
	bodyBytes, _ := ioutil.ReadAll(resp.Body)

	var resd Resdata
	if err := json.Unmarshal(bodyBytes, &resd); err != nil {
		panic(err)
	}

	return resd
}

func Register(name string, username string, email string, password string, source string, url string, token string) Resmsg {

	singupgame := &Usersignup{
		Name:     name,
		Username: username,
		Email:    email,
		Password: password,
		Source:   source,
	}

	client := &http.Client{}
	jsonReq, err := json.Marshal(singupgame)
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonReq))

	if err != nil {
		log.Fatalln(err)
	}

	req.Header.Add("Content-Type", "application/json; charset=utf-8")
	req.Header.Add("x-access-token", token)

	resp, err := client.Do(req)

	defer resp.Body.Close()
	bodyBytes, _ := ioutil.ReadAll(resp.Body)

	var resd Resmsg
	if err := json.Unmarshal(bodyBytes, &resd); err != nil {
		panic(err)
	}

	return resd
}
