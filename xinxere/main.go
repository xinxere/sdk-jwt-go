package main

import (
	"fmt"
	"xinxere/auth"
	"xinxere/user"
)

func main() {

	// Parameters Login(username string, password string)
	login := auth.Login("aurora7", "aurora7test", "http://localhost:8080/api/auth/signin")
	fmt.Printf("%+v\n", login)

	if login.Success {
		// Parameters Register(name string, username string, email string, password string, source string, url string, token string)
		usergameregister := user.Register("Test Aurora7", "testuser", "testuser@xinxere.com", "testaurora7", "aurora7", "http://localhost:8080/api/game/signup", login.Accesstoken)
		fmt.Printf("%+v\n", usergameregister)

		if usergameregister.Success {
			// Parameters Login(username string, password string, url string, token string)
			usergamelogin := user.Login("testuser", "testaurora7", "http://localhost:8080/api/game/signin", login.Accesstoken)
			fmt.Printf("%+v\n", usergamelogin)
		}

	} else {

		fmt.Println("Please Check User Auth!!!")
	}

}
